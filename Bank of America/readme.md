# Focus Account

## Company Background:
https://newsroom.bankofamerica.com/cathy-bessant
https://digiday.com/marketing/ai-isnt-just-technology-good-judgement-cathy-bessant-banks-need-humans/
https://www.americanbanker.com/news/bank-of-americas-cathy-bessant-the-most-powerful-woman-in-banking-2018


## Account Research:

Why's building & execution: How effectively do you determine the 3 Why's of doing a deal?
1. Why Gitlab
2. Why Now
3. Why Do Anything At All?


## Identify C-Suite:

1. Cathy Bessant - Chief Operations & Technology Officer
2. Dave Mathews, CIO Enterprise Architecture
3. Aditya Bhasin, CIO Retail Preferred Global Wealth & Investment Management (ie consumer banking)
4. David Reilly, CIO Global Banking and Markets


## Write C-Suite Letters

## Create Outreach Campaign for Directors


## Call Log:

* What do you know about Gitlab?
* Role
* Size 
* Situation
* Problem 
* Goals
* Budget
* Authority
* Timeline	
* Questions
* Next Steps

