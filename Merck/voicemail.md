Hello {{first_name}}, this is Kevin McKinley.

If you were in your office to take my call, this is what you would have heard...

The initiatives you're engaged in to (x) are why clients like Goldman Sachs is using the GitLab application.

I believe this topic could be important to you, and that’s why you’ll be hearing from me again next week on Tuesday, at 2:30 p.m., or, if you like, you can call me before then at 919.404.9198. Until then, have a wonderful weekend!

