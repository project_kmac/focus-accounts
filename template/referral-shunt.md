Ivy, thank you very much for introducing us. 

David, I’m looking forward to speaking with you. I don't want to assume that we can help, but a brief 15 min call will definitely help us figure it out. And at the end of that call, it's totally okay if you say no and let me know you're not interested or that it's not a good fit at this time.

You wouldn't happen to have an open 15 minute window for that conversation next week, would you?

