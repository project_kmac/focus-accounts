To: Marvin Ellison, CEO, Lowe’s
1000 Lowe’s Boulevard
Mooresville, NC 28117

24 January 2019

Subject: Why the CEOs of companies like Macy’s and Expedia use Gitlab to adopt enterprise-wide digital transformation
 
Warm greetings and happy New Year, Mr. Ellison,

I hope this missive finds you well. In my role as an SDR for GitLab, it is my responsibility to identify and research suitable partners who may benefit from our organization. 

During Lowe's 2018 Analyst & Investor Conference, you mentioned six key areas of focus which require IT system improvements. Those key areas of omni-channel, operational efficiency, and customer engagement are precisely what GitLab enables. I believe our application can accelerate your delivery of these improvements, just as we've done for clients like Macy's.

GitLab is touted as being the fastest and most reliable way for CEOs to enable digital transformation throughout their respective companies. As a concrete example, Goldman Sachs was in a similar situation with an outdated techonology toolset -- which meant they couldn't attract the best talent and were slow to innovate. Over the last year, they've migrated 5,000 developers to GitLab, which has resulted in an 84x increase in build productivity and allowed them to deploy faster than they ever thought possible.

Mr. Ellison, I believe there are at least four primary ways GitLab can help you drive value for your customers more quickly than your competitors.

1. We can improve your customer engagement (especially with the Pros) by aligning your IT deliverables with what the customers want
2. We can increase your operational efficiency, innovation velocity, and speed of delivery by having a Single Application for the entire Software Development Lifecycle
3. We can minimize IT costs and overhead which means less time spent integrating and maintaining your tools so you can Do It Right For Less
4. We can make Regulatory Compliance easy with an IT Audit Trail created by having a "Single Source of Truth"

If these outcomes sound like they might be relevant to your world, may I suggest we arrange a brief phone call? Together, we can quickly assess whether our companies would make for an ideal partnership. 

I will call your office on Tuesday, Feb 19 at 9:40 AM EST to make sure you've received this letter and answer any questions you may have. If this is an inconvenient time, kindly inform me as to your preferred availability.  

To your continued success,

Kevin McKinley
