"When you look at digital transformation, we are fundamentally focused on four different things. One is customer experience. And customer experience is a whole lot more than just a great website or a great mobile application. 

Our second strategy is around productivity and efficieny and improvements. 

The third thing is expanding our ecosystem. We think of ourselves as a player that is providing a service - what we want to do is be at the center of the customer experience. It does not matter who provides the service to the customer, as long as we manage and are central to how that service is provided. 

The last thing in terms of our strategy is building out a growth platform. When you look at insurance companies in general, we all are burdened with legacy technology. We have this approach where while we are still trying to address working around the legacy environment - we call it the "digital surroung strategy" - in parallel with that we are investing in much more nimble platforms that allow us to price new products significantly faster than we have done in the past.

I think of technology as a value enabler. Always think about the end customer first. If you do the right thing for the end customer, other things will naturally fall into place. What drives me and what I believe should drive individuals in my department, is doing the right thing for the end customer. The second is around teamwork and collaboration. As a team, we can achieve much greater things. Collaboration both within IT, and between IT and the business becomes really important. Fiduciary responsibility is really important. Make good decisions, take risks, if you're going to fail, fail quickly and move on."


****

To: Puneet Bhasin, EVP, CIO, & CDO, Unum
UNUM
842 West Sam Houston Parkway North
Houston, TX 77024

1 Fountain Square
Chattanooga, TN 37402

February 1, 2019

Subject: Why do companies like Goldman Sachs use Gitlab? Because it's the fastest way for CIOs to deliver a digital transformation

Warm greetings Mr. Bhasin,

I hope the new year has treated you well. In my role as an SDR for GitLab, it is my responsibility to identify and research suitable partners who may benefit from our organization. 

During a recent interview with Straight Talk, you mentioned that Unum's digital transformation is focused on four key areas (customer experience, productivity and efficiency, expanding your ecosystem, and building out a growth platform). These initiatives are precisely what GitLab's application enables - and is why it is touted as being the fastest and most reliable way for CIOs to enable digital transformation throughout their respective organization.

As a concrete example, Goldman Sachs was in a similar scenario with regards to having a legacy environment -- which meant they couldn't attract the best talent and were slow to innovate. Over the last year, they've migrated 5,000 developers to GitLab, which has resulted in an 84x increase in build productivity and allowed them to be more nimble and produce faster than they ever thought possible.

*“Our teams went from two weeks releases to six times per day using GitLab because they do not need to wait for infrastructure." Andrew Knight, Managing Director, Goldman Sachs*

Mr. Bhasin, I believe there are at least five primary ways GitLab can help you drive value for your customers more quickly than your competitors.

1. We can enable better collaboration between IT and the business, resulting in alignment of your IT deliverables with business value and doing the right thing for the end customer  
2. We can improve your productivity and operational efficiency by having a Single Application for the entire Software Development Lifecycle, which means being able to produce new products significantly faster
3. We can help you be fiscally responsible by reducing IT costs and overhead which also means less time spent integrating and maintaining your tools
4. We can enhance information security and remediate vulnerabilities with automated testing and code scanning
5. We can make Regulatory Compliance  easy with an IT Audit Trail created by having a "Single Source of Truth"

If these outcomes sound like they might be relevant to your world, then a brief phone call may be in order. Together, we can quickly assess whether your objectives and our solution would make for an ideal partnership. I will call your office on Friday, Feb 1 at 9:00 AM EST. If this is an inconvenient time, kindly inform me as to your preferred availability. 

To your continued success,

Kevin McKinley

