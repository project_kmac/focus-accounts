Eric Rector
Director, IT Delivery
1 Fountain Square
Chattanooga, TN 37402

GitLab is an application for the entire SLDC used by clients like the US Air Force.

Typically, the IT Directors I work with in an IT Delivery or Application Development role tell me this...

They've got an experienced IT team that's using Agile/Scrum methodologies and they've greatly improved speed of delivery. But occasionally, they still have these SILOs that creep in that block collaboration, which can lead to missed deadlines.

Or maybe that's not a problem because they've built a great toolchain. They're probably using GitHub, Docker, Jenkins, maybe even some of the CA tools like AgileCentral. But then now, they have so many applications in their stack, that maybe they're a little concerned about having to maintain, update, and manage all of those tools and integrations.

Does any of that sound like it might be relevant to your world, and worth a brief 3-5 minute conversation?