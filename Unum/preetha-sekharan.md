https://www.linkedin.com/in/preetha-sekharan-3b32b14/

•	Co-designed an IT Operating Model overhaul to drive greater business alignment and agility for a $350M IT organization
•	Established a strategy team to further digital advancements working in close partnership with business development, corporate and business unit strategies 
•	Launched an Incubation Lab to test and experiment with emerging technologies / business ideas in a fail-fast, fail-cheap and risk-free environment; Instituted processes to industrialize successful experiments
•	Revitalized architecture and engineering practices with focus on talent development , building financial technical and business acumen, standardizing artifacts and blueprints
•	Redefined and formalized governance processes around architectural and investment decisions
See less



To: Preetha Sekharan, VP of Digital Strategy & Transformations
Unum
Six Concourse Parkway
Atlanta, GA 30328


February 4, 2019

Subject: Why do companies like Goldman Sachs and Capgemini use Gitlab? Because it's the fastest way to deliver a digital transformation

Warm greetings Ms. Sekharan,

I hope the new year has treated you well. In my role as an SDR for GitLab, it is my responsibility to identify and research suitable partners who may benefit from our organization. 

The initiatives you are engaged in to guide Unum's Digital Transformation and Strategy, as well as drive business alignment and agility are precisely what the GitLab application enables.

I bring this to your attention because I believe that utilization of GitLab within Unum can dramatically accelerate the delivery of your digital transformation and proposed objectives. Those key areas of innovation, business alignment, and agility, is why GitLab is used by clients like ING Bank, BNY Mellon, and Freddie Mac.

As a concrete example, Goldman Sachs was in a similar scenario with their digital transformation. Over the last year, they've migrated 5,000 developers to GitLab, which has resulted in an 84x increase in build productivity and allowed them to be more Agile and produce faster than they ever thought possible.

*“Our teams went from two weeks releases to six times per day using GitLab because they do not need to wait for infrastructure." Andrew Knight, Managing Director, Goldman Sachs*

Ms. Sekharan, I believe there are at least five primary ways GitLab can help you drive value for your customers and stakeholders.

1. We can enable better collaboration between IT and the business, resulting in alignment of your IT deliverables with business value and doing the right thing for the end customer  
2. We can improve your productivity and operational efficiency by having a Single Application for the entire Software Development Lifecycle, which means being able to produce new products significantly faster
3. We can help you be fiscally responsible by reducing IT costs and overhead which also means less time spent integrating and maintaining your tools
4. We can enhance information security and remediate vulnerabilities with automated testing and code scanning
5. We can make Regulatory Compliance easy with an IT Audit Trail created by having a "Single Source of Truth"

If these outcomes sound like they might be relevant to your world, then a brief phone call may be in order. Together, we can quickly assess whether your objectives and our solution would make for an ideal partnership. I will call your office on Friday, Feb 8 at 9:30 AM EST. If this is an inconvenient time, kindly inform me as to your preferred availability. 

To your continued success,

Kevin McKinley

