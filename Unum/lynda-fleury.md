https://www.youtube.com/watch?v=nZl2QBKQ7tE

https://www.youtube.com/watch?v=rrpD64FWzI0

http://eval.symantec.com/mktginfo/enterprise/articles/b-ciodigest_april09_cover.en-us.pdf

I think my biggest challenges today are 10,000 hands on the keyboard and being one click away from a security breach or data breach. And if I had an opportunity to do things differently with a different skillset and budgets, I'd start looking at more automation to be able to reduce that risk significantly.

I think my investments in 2019 are going to be limited. I think we're going to look for orchestration and automation, and maybe a technology for an identity and access management perspective that allows us to do near real time provisioning and risk based provisioning. But I really want my team focusing on getting the most out of the investments that we've already made. 

I think my biggest challenge looking forward to shifts in technology are really the internet of things. 

Dream job: Wildlife photographer.

Has an African Grey parrot that is learning some of the Alexa skills. 

****

To: Lynda Fleury, Global CISO
Unum
1 Fountain Square
Chattanooga, TN 37402


February 1, 2019

Subject: Why companies like Goldman Sachs use GitLab to defend and secure all aspects of their Information Security

Subject: How do companies like Goldman Sachs secure technology with 10,000 hands on a keyboard? Using GitLab


Warm greetings Ms. Fleury,

I hope this missive finds you well. In my role as an SDR for GitLab, it is my responsibility to identify and research suitable partners who may benefit from our organization. 

During a recent interview for the ChaTech CxO Auction, you mentioned that one of your biggest challenges is being 1 click away from a security or data breach. The initiatives you are engaged in with regards to automation and orchestration to reduce that risk significantly, are precisely what the GitLab application enables.

I bring this to your attention because I believe that utilization of GitLab within Unum can dramatically assist your organization in orchestrating Unum's security infrastructure and protecting critical customer information. GitLab is touted as being the fastest and most reliable way for CISOs to shift security left throughout their respective companies, and is one of the many reasons why Goldman Sachs has adopted our solution.

Ms. Fleury, I believe there are at least three primary ways GitLab can help you defend and secure Unum's information technology while continuing to drive value for your stakeholders.

1. With GitLab, you no longer need choose between velocity and risk. Because it’s not an incremental cost, you can test every code change, not just critical apps or annual scans. You can test ALL of your code, on every commit, automatically, with real time provisioning.
2. We can help you better leverage scarce security resources by putting application security tools, that are meant for the developer, into the hands of the developer. This means that those 10,000 hands on a keyboard can remediate more code and earlier than possible with traditional application security tools.
3. We can make regulatory compliance and governance reporting easy with an IT Audit Trail created by having a "Single Source of Truth"


If these outcomes sound like they might be relevant to your world, then a brief phone call may be in order. Together, we can quickly assess whether your objectives and our solution would make for an ideal partnership. I will call your office on Friday, Feb 1 at 9:00 AM EST. If this is an inconvenient time, kindly inform me as to your preferred availability. 

To your continued success,

Kevin McKinley

