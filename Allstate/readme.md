#Allstate 

## Company Background:

Allstate
2775 Sanders Road
Northbrook, IL 60062 US


## Account Research:

Why's building & execution: How effectively do you determine the 3 Why's of doing a deal?
1.  Why Gitlab
2.  Why Now
3. Why Do Anything At All?


## Identify C-Suite:

0. Chris Gates, CTO - https://www.linkedin.com/in/gateschris/
1. Jeffrey Wright - Vice President & Chief Information Security Officer - http://www.linkedin.com/in/jeffreywrightcissp
1. Suren Gupta - EVP Technology and Strategic Ventures at Allstate Insurance Company - https://www.linkedin.com/in/suren-gupta-900b5b1/


1. Patricia Lawicki, Chief Information Officer

### VPs

2. Keith Hauschildt Executive VP, Operations & Technology & Chief Operating Officer (Allstate Benefits)

2. Mia Boom-Ibes
2. Joe Skala - https://www.linkedin.com/in/joeskala/
2. Carla Zuniga - https://www.linkedin.com/in/carla-zuniga-117b451 
2. Robert Gillman Vice President Testing and Release Management - https://www.linkedin.com/in/robert-gillman-296514/
2. Robert Borland - Vice President, Support, Infrastructure, & Technical Services
2. Sreenivas Pondicherry - Vice President , Architecture, Data & Strategy
2. Kathy Rubash - Vice President, Program Management Office & Agile Transformation Strategy
2. Rashmi Tripathi - Senior Vice President - Allstate Brand Operations https://www.linkedin.com/in/rashmi-tripathi-787b5362/
2. Kery Ruhl - Vice President & Chief Risk Officer, Information Technology - RETIRED AUG 2018
2. Tony Luciano - Vice President, Information Technology (Dealer Services) - https://www.linkedin.com/in/tony-luciano-06b24527
2. Joshua Milberg - Vice President, Technology Strategy & Transformation - https://www.linkedin.com/in/joshua-milberg-92a49b1/
2. Howard Hayes Senior VP, Product & Service Innovation & Development
Allstate Corporation
3806 N Ashland Ave
Chicago, IL 60613 US


* David Dickson Vice President, Enterprise Solutions (Allstate Benefits) https://www.linkedin.com/in/david-dickson-8478a13/
Allstate
4131 Southside Boulevard
Jacksonville, FL 32216 US


1. Roger Hartmuller (Lisa Gisler is EA) Vice President , Divisional Chief Information Officer, Enterprise Shared Services - https://www.linkedin.com/in/roger-hartmuller-2a376a2


### CIOs

1. Kamal Natarajan - https://www.linkedin.com/in/kamal-natarajan-4551742/
1. Pete Corrigan Chief Information Officer, Allstate Personal Lines
1. John White (CIO, Business Insurance)
1. Jerry Higgins (CIO, Ivantage)
1. Jeff Sargent (CIO, Allstate Investments)
1. Scott McConnell, Managing Director , Technology & Divisional Chief Information Officer (Allstate Investments) - https://www.linkedin.com/in/scott-mcconnell-40a2924
1. Mary Springberg - CIO Allstate Financial 
1. Pranava Doctor - Divisional Chief Information Officer (Marissa Coleman is EA)


### Directors

* Cassandra Lems
* Mike Fortuna
* Nick Giannakopoulos
* Patrick Soule - Director, Information Technology (Allstate Roadside Services) https://www.linkedin.com/in/patrick-soule-82033b6
* Raghu Parthasarathy - Director, Production Operations https://www.linkedin.com/in/raghu-parthasarathy-7bb7936
* Charlotte Brubaker - Director, Project and Product Management https://www.linkedin.com/in/charlotte-brubaker-6613152
* Gregory Baldwin https://go.discoverydb.com/eui/#/person/1900
* Jennifer Hansen - https://www.linkedin.com/in/jennifer-hansen-a400a372





## Create Outreach Campaign for Directors


## Call Log:

11/19/18   Ajay Hiremath

-> Uses GitHub, Jenkins, Artifactory from Enterprise defined tooling. IPSoft provides tooling for Amelia (AI Virtual Assistant) - https://www.ipsoft.com/amelia/


Previously worked on the Enterprise Infrastructure defining DevOps tooling, now working on AI (Applications Side)

4 Scrum teams of 9 each; currently trying to remove unnecessary folks like UX/BA -> (Leads me to ask, Sean, what does a Scrum team look like? I see a lot of Business Analysts and never really understood what role they play)

New role, Sr Manager  (2 mos in) for Ajay working on the AI virtual assistant called Amelia
He was with the enterprise group that established the tool chain focused on CI/CD devops enablement and transition from waterfall to Agile
Devops toolchain complete - using GitHub, Jenkins, Artifactory 
