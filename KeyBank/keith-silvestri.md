Keith Silvestri
CTO
KeyBank
127 Public Square
Cleveland, OH 44114

Warm greetings Mr. Silvestri,

I hope this missive finds you well, and that the new year has treated you kindly. In my role as an SDR for GitLab, it is my responsibility to identify and research suitable partners who may benefit from our organization. 

During a recent press release, you mentioned that Key will be leveraging Google Cloud Services Platform and Kubernetes to bring containerized applications into your data centers. The initiatives your are involved in to utilize GCP, Kubernetes, and containerized applications are precisely what our application enables. Clients like Freddie Mac, BNY Mellon, and ING Bank use GitLab to deploy to the cloud, deliver capabilities faster, and make systems more reliable & secure.

As a concrete example, Goldman Sachs had a legacy environment which meant they were slow to innovate, and wanted to move to the cloud. Over the last year, they've deployed GitLab to over 5,000 developers, which has resulted in an 84x increase in build productivity, allowed them to be more nimble and produce faster than they ever thought possible, because they were able to utilize Kubernetes and Google Cloud Platform.

“Our teams went from two weeks releases to six times per day using GitLab because they do not need to wait for infrastructure. GitLab has allowed us to dramatically increase the velocity of development in our Engineering Division. We believe GitLab’s dedication to helping enterprises rapidly and effectively bring software to market will help other companies achieve the same sort of efficiencies we have seen inside Goldman Sachs. We now see some teams running and merging 1000+ CI feature branch builds a day!” Andrew Knight, Managing Director & Technology Fellow at Goldman Sachs

“We’re bringing into the firm a platform that our engineers actually want to use – which helps drive adoption across multiple teams and increase productivity without having to ‘force’ anyone to adopt it. This is really helping to create an ecosystem where our end users are actively helping us drive towards our strategic goals - more releases, better controls, better software.”
George Grant, VP & Technology Fellow, Goldman Sachs

Mr. Silvestri, I believe there are at least four primary ways GitLab can help you drive value for your customers and continue to lead Key’s digital transformation journey.

1. We can improve your productivity and operational efficiency through our Single Application for the entire Software Development Lifecycle. This means increasing speed to market, delivering products faster, and aligning those IT deliverables with business value - all without having to rip & replace.
2. We can help you reduce IT licensing costs and maintenance overhead which also means less time spent maintaining your tools. Your teams can manage their Docker containers, orchestrate their Kubernetes clusters, and automate your pipeline all within one application. 
3. We can enhance information security and remediate vulnerabilities with automated testing and code scanning. With GitLab, you no longer need to choose between velocity and risk. Because it’s not an incremental cost, you can test every code change, not just critical apps or annual scans. You can test ALL of your code, on every commit, automatically, with real time provisioning.
4. We can make Regulatory Compliance and governance reporting easier with an IT Audit Trail created by having a "Single Source of Truth"

In closing, if these outcomes sound like they might be relevant to your world, I would like to suggest for us to arrange a brief phone call. Together, we can quickly assess whether our companies would make for an ideal partnership. 

I will call your office on Tuesday, Feb 26 at 9:00 AM EST to ensure you've received this letter and answer any questions you may have. If this is an inconvenient time, kindly inform me as to your preferred availability.  

To your continued success,




Kevin McKinley

****

We are creating a new kind of bank for our customers, and Google's creation of Kubernetes and Istio made them the obvious cloud to collaborate with as we look to bring containerized applications into our data centers," said Keith Silvestri, Chief Technology Officer. "Using Cloud Services Platform for our hybrid deployment will be like having hundreds of Google developers all working to build us the best platform for delivering modern banking services to our customers."

