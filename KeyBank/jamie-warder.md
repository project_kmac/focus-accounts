Jamie Warder
Head of Business Banking and Investments
KeyBank
127 Public Square
Cleveland, OH 44114


February 15, 2019

Warm greetings Mr. Warder,

I hope this missive finds you well, and that the new year has treated you kindly. In my role as an SDR for GitLab, it is my responsibility to identify and research suitable partners who may benefit from our organization. 

During your interview with Sonny Singh at OIC 2018, you mentioned that speed matters with the velocity of the change that's coming at all of us. I bring this to your attention because the initiatives you are engaged in to modernize architecture and enable a digital transformation process are precisely why GitLab's application was built. This is also why it is touted as being the fastest and most reliable way for CIOs to enable digital transformation throughout their respective organization.

As a concrete example, Goldman Sachs was in a similar scenario with regards to having a legacy environment which meant they were slow to innovate. Over the last year, they've migrated 5,000 developers to GitLab, which has resulted in an 84x increase in build productivity and allowed them to be more nimble and produce faster than they ever thought possible.

“Our teams went from two weeks releases to six times per day using GitLab because they do not need to wait for infrastructure." Andrew Knight, Managing Director, Goldman Sachs

Mr. Warder, I believe there are at least five primary ways GitLab can help you drive value for your customers more quickly than your competitors.

1. We can improve your productivity and operational efficiency through having a Single Application for the entire Software Development Lifecycle, which means increasing speed to market and delivering products faster
2. We can enable the technology ecosystem that allows you to go faster, and respond quickly

better collaboration between IT and the business Building , resulting in alignment of your IT deliverables with business value  

3. We can help you be fiscally responsible by reducing IT costs and overhead which also means less time spent integrating or maintaining your tools, and avoiding the creation of yet another legacy system down the road
4. We can enhance information security and remediate vulnerabilities with automated testing and code scanning
5. We can make Regulatory Compliance and governance reporting easier with an IT Audit Trail created by having a "Single Source of Truth"


Finally, during Key's 2018 Investor Day, you spoke about having to be very targeted about whom you choose to innovate with and being specific about where to buy, build, or partner. I understand that discretion with whom you work with is of the utmost importance. As such, if these outcomes sound like they might be relevant to your world, may I suggest we arrange a brief phone call? Together, we can quickly assess whether our companies would make for an ideal partnership. 

I will call your office on Tuesday, Feb 19 at 9:40 AM EST to make sure you've received this letter and answer any questions you may have. If this is an inconvenient time, kindly inform me as to your preferred availability.  

To your continued success,

Kevin McKinley

*****