# Focus Account

## Company Background:
http://investor.key.com/investor-overview

https://www.google.com/search?q=amy+brady+transcript+keybank&source=lnms&tbm=vid&sa=X&ved=0ahUKEwiNi9X1mI3gAhUkNH0KHQbmBsEQ_AUIEygA&biw=1785&bih=930

200 year old institution. 13th largest super-regional financial services company in the United States.

First Niagra was the largest acquisition post-financial crisis. Oct 2016 we converted First Niagra to KeyBank 100% over the weekend which required converting over 240 systems into the KeyBank ecosystem. We also at the same time were undergoing some really transformational work with Oracle on what we called our Digital 17 program, which was re-architecting 

## Account Research:

Why's building & execution: How effectively do you determine the 3 Why's of doing a deal?
1. Why Gitlab
2. Why Now
3. Why Do Anything At All?

"Key is on a journey to Digitize the Enterprise." Beth Mooney

"We mean the end-to-end experience. From the portal through the back-office. We're transforming Key to become a digital enterprise."
"The progress we're making on modernization, services, and capabilities. It's hard to benchmark specifically,"

Q: "How much do you spend on "

We spend about 7-800 million on tech. 200 million in investments. We will continue to invest in technology. 

JP Morgan spends 11 billion on tech. Don't you have a tough job? 

"We will never be able to spend the raw dollars that they do. So we have to be very targeted on who we choose to innovate with. We can't innovate with 6 partners around the same capability - we have to choose one. We have to be very specific about where we're going to buy vs build and partner. And quite frankly, when you do things like take on an industry  "


## Identify C-Suite:

1. Amy Brady - CIO
2. Beth Mooney - CEO
3. Kelly Uhrich - CISO
	* Ginene Mozzone - VP, Security Risk & Compliance
	* Chris Campbell - Director, Information Security Architecture & Operations

4. Keith Silvestri - CTO
5. Mike Onders CIO
6. Kim Snipes - CIO (Moved from USAA Jan 2019)


* Jason Rudman - https://www.linkedin.com/in/jasonrudman/
* Megan Kakani - https://www.linkedin.com/in/megan-kakani-9b333215/
* Jamie Warder - 

* John Rzeszotarski
* Chris Campbell
* Ravi Chirumamilla
* Karen Heathwaite https://www.linkedin.com/in/karen-heathwaite-3698377/
* Frank Polino
* Kathy Dockery
* Jennifer Zorn - SVP, Director of Business Performance & Continuous Improvement at KeyBank Mortgage https://www.linkedin.com/in/jenniferzorn/
* Shelby Ball - EVP https://www.linkedin.com/in/shelby-ball-b477321a/ Technology and Operations / Business Management and Optimization Executive


* Dale Jablonski - SVP, Enterprise Architecture

* Keith Bell, VP, Enterprise Technology Operations
* Margie O'Connor, VP Technical Project Management

* Brian Akers - Director, Infrastructure Engineering
* Michael Slama - SVP, Business Technology
* Douglass Kanouff, Enterprise Architecture

* Jeff Semonovich, CIO (Corporate Bank)


## Write C-Suite Letters

## Create Outreach Campaign for Directors


## Call Log:

* What do you know about Gitlab?
* Role
* Size 
* Situation
* Problem 
* Goals
* Budget
* Authority
* Timeline	
* Questions
* Next Steps

