https://www.youtube.com/watch?v=6It9heK9MU8

https://www.youtube.com/watch?v=wN0xKqY2Dg8

http://fortune.com/2018/01/10/keycorp-ceo-beth-mooney/

https://www.wgrz.com/video/news/local/extended-interview-with-keybank-ceo-beth-mooney/71-2787380 <- (This interviewer is horrible.)

https://www.forbes.com/sites/jennagoudreau/2011/09/06/key-corp-ceo-beth-mooney-makes-banking-history-power-women/

https://www.youtube.com/watch?v=JaOJeFXsDwY

https://www.youtube.com/watch?v=d24xhVte80s

https://www.crainscleveland.com/article/20180801/news/170381/keycorps-beth-mooney-among-bankings-highest-paid-ceos

https://www.youtube.com/watch?v=4tBwjDeV1qk

https://www.youtube.com/watch?v=xn37TUl1G34

https://www.americanbanker.com/video/women-have-an-obligation-to-help-other-women-succeed-keybanks-mooney

https://www.courant.com/business/93229173-132.html?trb=20181212

"What' I'd like to see us do in three to five years is I'd like to be a top 5 bank in connecticut and that's what we're here to do."

Beth Mooney
CEO & Chairman
KeyBank
127 Public Square
Cleveland, OH 44114


February 8, 2019

Warm greetings Ms. Mooney,

I hope this missive finds you well, and that the new year has treated you kindly. In my role as an SDR for GitLab, it is my responsibility to identify and research suitable partners who may benefit from our organization. 

During a recent interview with Straight Talk, you mentioned that Unum's digital transformation is focused on four key areas (customer experience, productivity and efficiency, expanding your ecosystem, and building out a growth platform). These initiatives are precisely what GitLab's application enables - and is why it is touted as being the fastest and most reliable way for CIOs to enable digital transformation throughout their respective organization.

As a concrete example, Goldman Sachs was in a similar scenario with regards to having a legacy environment -- which meant they couldn't attract the best talent and were slow to innovate. Over the last year, they've migrated 5,000 developers to GitLab, which has resulted in an 84x increase in build productivity and allowed them to be more nimble and produce faster than they ever thought possible.

“Our teams went from two weeks releases to six times per day using GitLab because they do not need to wait for infrastructure." Andrew Knight, Managing Director, Goldman Sachs

Mr. Bhasin, I believe there are at least five primary ways GitLab can help you drive value for your customers more quickly than your competitors.

1. We can enable better collaboration between IT and the business, resulting in alignment of your IT deliverables with business value and doing the right thing for the end customer  
2. We can improve your productivity and operational efficiency by having a Single Application for the entire Software Development Lifecycle, which means being able to produce new products significantly faster
3. We can help you be fiscally responsible by reducing IT costs and overhead which also means less time spent integrating and maintaining your tools
4. We can enhance information security and remediate vulnerabilities with automated testing and code scanning
5. We can make Regulatory Compliance easy with an IT Audit Trail created by having a "Single Source of Truth"

If these outcomes sound like they might be relevant to your world, let us arrange a brief phone call, and together, we can quickly assess whether our companies would make for an ideal partnership. 

I will call your office on Friday, Feb 1 at 9:00 AM EST. If this is an inconvenient time, kindly inform me as to your preferred availability. 

To your continued success,

Kevin McKinley