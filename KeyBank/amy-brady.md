https://www.youtube.com/watch?v=VzbuaaFVQxY

https://www.americanbanker.com/2018-women-in-banking-gala

https://www.linkedin.com/in/ambrady/

https://www.forbes.com/sites/oracle/2018/07/30/keybank-charges-ahead-with-three-pronged-digital-modernization/#5709dee66d1b

https://www.youtube.com/watch?v=ROIOCo0PBHQ

https://www.americanbanker.com/video/what-is-keycorps-data-factory

https://www.youtube.com/watch?v=tUYuh5bsHMg
As you said, the velocity of the change that's coming at all of us, the lifecycle if you will of the technologies we're deploying is shortening, and so we've got to be smart, but you also have to try lots of different things. So I go back to we are looking at a multi-pronged approach in how do we partner with fintechs around what capabilities they are delivering, how do we enable our teams to get creative and really explore and create on their own to see what they can deliver, and how do we then bring those things to market. So speed matters. Analytics matters, but speed matters as well. And that is going to be differentiator for those of us who are successful in five yers, three years, whatever that is. Building that technology ecosystem that allows you to go faster, and respond quickly, and then maybe change course is equally as important as what technology you're actually delivering.

Amy Brady
CIO
KeyBank
127 Public Square
Cleveland, OH 44114

February 15, 2019

Warm greetings Ms. Brady,

I hope this missive finds you well, and that the new year has treated you kindly. In my role as an SDR for GitLab, it is my responsibility to identify and research suitable partners who may benefit from our organization. 

During your interview with Sonny Singh at OIC 2018, you mentioned that speed matters with the velocity of the change that's coming at all of us. I bring this to your attention because the initiatives you are engaged in to modernize architecture and enable a digital transformation process are precisely why GitLab's application was built. This is also why it is touted as being the fastest and most reliable way for CIOs to enable digital transformation throughout their respective organization.

As a concrete example, Goldman Sachs was in a scenario with regards to having a legacy environment which meant they were slow to innovate. Over the last year, they've migrated 5,000 developers to GitLab, which has resulted in an 84x increase in build productivity and allowed them to be more nimble and produce faster than they ever thought possible.

“Our teams went from two weeks releases to six times per day using GitLab because they do not need to wait for infrastructure. GitLab has allowed us to dramatically increase the velocity of development in our Engineering Division. We believe GitLab’s dedication to helping enterprises rapidly and effectively bring software to market will help other companies achieve the same sort of efficiencies we have seen inside Goldman Sachs. We now see some teams running and merging 1000+ CI feature branch builds a day!” Andrew Knight, Managing Director & Technology Fellow at Goldman Sachs


“We’re bringing into the firm a platform that our engineers actually want to use – which helps drive adoption across multiple teams and increase productivity without having to ‘force’ anyone to adopt it. This is really helping to create an ecosystem where our end users are actively helping us drive towards our strategic goals - more releases, better controls, better software.”
George Grant, VP & Technology Fellow, Goldman Sachs


Ms. Brady, I believe there are at least four primary ways GitLab can help you drive value for your customers more quickly than your competitors.

1. We can improve your productivity and operational efficiency through our Single Application for the entire Software Development Lifecycle. This means increasing speed to market, delivering products faster, and aligning those IT deliverables with business value - all without having to rip & replace.
2. We can help you reduce IT licensing costs and maintenance overhead which also means less time spent maintaining your tools.
3. We can enhance information security and remediate vulnerabilities with automated testing and code scanning. With GitLab, you no longer need to choose between velocity and risk. Because it’s not an incremental cost, you can test every code change, not just critical apps or annual scans. You can test ALL of your code, on every commit, automatically, with real time provisioning.
4. We can make Regulatory Compliance and governance reporting easier with an IT Audit Trail created by having a "Single Source of Truth"

Additionally, during Key's 2018 Investor Day, you spoke about having to be very targeted about whom you choose to innovate with and being specific about where to buy, build, or partner. I respect that discernment of whom you work with is of the utmost importance and can assure you of our desire to be excellent partners with Key (as we have with our other clients). 

In closing, if these outcomes sound like they might be relevant to your world, I would like to suggest for us to arrange a brief phone call. Together, we can quickly assess whether our companies would make for an ideal partnership. 

I will call your office on Tuesday, Feb 26 at 9:00 AM EST to make sure you've received this letter and answer any questions you may have. If this is an inconvenient time, kindly inform me as to your preferred availability.  

To your continued success,

Kevin McKinley

*****

https://video.oracle.com/detail/videos/featured-videos/video/4909989550001/sonny-singh-on-digital-strategy-with-amy-brady-of-keybank


"It was very clear we had an opportunity to modernize our digital ecosystem. We were a lot behind in our digital journey. But it was exciting because we had the support of our board, the support of our executives, and we had a very talented technology team."

"We want digital not to lead the way, but to enable the business value. We want a holistic digital transformation." 

"When we started talking to Oracle, we started to see synergies. We couldn't take a product out approach, or a systems out approach."

"If we do this, you have to commit to me you'll be on this progressive journey with us. Because I'm not going to do a rip and replace approach. Those days are gone, there's no financial business case for that."

We started this journey 18 months ago, a lot has changed in those 18 months.

It had to be architecturally driven. We knew if it wasn't architecturally driven, then we would be sitting here 5 years, 10 years later and going, Okay, this is just the new legacy. And that was not the legacy I wanted to leave for our institution. 

Key has not been an acquisitive company. We have been buying capabilities and technologies. 

We were constantly working with our partners to say, "How do you bring value while modernizing?"

Part of the reason we chose the Oracle platform was because we knew we would be able to plug other things into it quickly. That has proven true.

What this positions us to do is come out of this transition and really go, "What's next?"

What are the next priorities for our company and be responsive to that?

A lot of companys just use lipstick on the pig. The reality is a lot of our business partners and people with the consumerization of IT, they really just want the next APP and that's digitization to them. I have done a lot of work educating our board of directors that Digitization has to go through all the way through to these processes for us to realize the true full value. And so I would say that the consumer lending space is clearly a space for us where we have too many systems, have customized things just a bit to make things work, to build our own workflows that over time have become cumbersom. And just we realize are not as nimble as we need them to be.

The other place that's really intriguing to us is the whole concept of the holy grail of originzations. I like your term frictionless. 

We feel very proud about truly bringing the whole experience to the multi channel.

The last piece is the data that powers all of this so that you can bring the right analytics to bear.

****

https://video.oracle.com/detail/video/5779676036001/oic-2018:-financial-services---amy-brady-keybank?autoStart=true&q=keybank

First Niagra was the largest acquisition post-financial crisis. Oct 2016 we converted First Niagra to KeyBank 100% over the weekend which required converting over 240 systems into the KeyBank ecosystem. We also at the same time were undergoing some really transformational work with Oracle on what we called our Digital 17 program, which was re-architecting from stem to stern our digital platforms.

For all of us that are older than a startup, we have got to be able to run with these legacy systems, transform and modernize, and at the same time work with the most modern of technologies to bring that customer experience you're talking about.

We innovate with a DevOps team that is constantly thinking about how do we do continuous deployment where we can and pushing the envelope to how we bring that to some of our systems that aren't architected that way. We also innovate with partners like Oracle, and fintechs.

We've got lots of examples where we're partnering with startups, midsize, and more mature fintechs, to be able to deliver, co-innovate together. 

Most of us don't want to have to touch our core platforms. It's hard to find a business case for that. One of the things we went through with our integration with First Niagra, it clearly exposed some of the age of our core systems and the complexity that had been built up over years and years. 

As you said, the velocity of the change that's coming at all of us, the lifecycle if you will of the technologies we're deploying is shortening, and so we've got to be smart, but you also have to try lots of different things. So I go back to we are looking at a multi-pronged approach in how do we partner with fintechs around what capabilities they are delivering, how do we enable our teams to get creative and really explore and create on their own to see what they can deliver, and how do we then bring those things to market. So speed matters. Analytics matters, but speed matters as well. And that is going to be differentiator for those of us who are successful in five yers, three years, whatever that is. Building that technology ecosystem that allows you to go faster, and respond quickly, and then maybe change course is equally as important as what technology you're actually delivering.

In 18 months, we will be back here telling you that our lending transformation with was a huge success, our financial crimes transformation was a huge success. I should've mentioned we're about to go live in two weeks with our new cash/currency transaction, we will have a new Know Your Customer system thanks to Oracle, this year. In 18 months those things will be complete.

How do we deliver now those capabilities that we are offering self-service to our clients that they don't have today. How do we bring it to the user experience layer in a way that our clients want to consume. 

*****

http://investor.key.com/investor-overview

"Key is on a journey to Digitize the Enterprise." Beth Mooney

"We mean the end-to-end experience. From the portal through the back-office. We're transforming Key to become a digital enterprise."
"The progress we're making on modernization, services, and capabilities. It's hard to benchmark specifically,"

Q: "How much do you spend on "

We spend about 7-800 million on tech. 200 million in investments. We will continue to invest in technology. 

JP Morgan spends 11 billion on tech. Don't you have a tough job? 

"We will never be able to spend the raw dollars that they do. So we have to be very targeted on who we choose to innovate with. We can't innovate with 6 partners around the same capability - we have to choose one. We have to be very specific about where we're going to buy vs build and partner. And quite frankly, when you do things like take on an industry  "
