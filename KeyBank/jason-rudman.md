Jason Rudman
EVP, Head of Consumer Payments and Digital
KeyBank
127 Public Square
Cleveland, OH 44114

Warm greetings Mr. Rudman,

I hope this missive finds you well, and that the new year has treated you kindly. In my role as an SDR for GitLab, it is my responsibility to identify and research suitable partners who may benefit from our organization. 

Excited to hear that you're going to be presenting at Digital Banking in June. The initiatives you're engaged in to innovate, optimize end-to-end customer experiences, develop business compliance oversight, and implement product strategies that drives bottom-line results are precisely what our application enables. GitLab is used by clients like Goldman Sachs, ING Bank, and BNY Mellon to deliver faster, innovate more, and reduce costs. 

As a concrete example, Goldman Sachs had legacy environment which meant they were slow to innovate. Over the last year, they've migrated 5,000 developers to GitLab, which has resulted in an 84x increase in build productivity and allowed them to be more nimble and produce faster than they ever thought possible.

“Our teams went from two weeks releases to six times per day using GitLab because they do not need to wait for infrastructure." Andrew Knight, Managing Director, Goldman Sachs

Mr. Rudman, I believe there are at least four primary ways GitLab can help you drive value for your customers more quickly than your competitors.

1. We can improve your productivity and operational efficiency through our Single Application for the entire Software Development Lifecycle. This means amplifying innovation, increasing speed to market, delivering products faster, and aligning those IT deliverables with business value - all without having to rip & replace.
2. We can help you reduce IT licensing costs and maintenance overhead which also means less time spent maintaining your tools. Your teams can manage their Docker containers, orchestrate their Kubernetes clusters, and automate your pipeline all within one application. 
3. We can enhance information security and remediate vulnerabilities with automated testing and code scanning. With GitLab, you no longer need to choose between velocity and risk. Because it’s not an incremental cost, you can test every code change, not just critical apps or annual scans. You can test ALL of your code, on every commit, automatically, with real time provisioning.
4. We can make Regulatory Compliance and governance reporting easier with an IT Audit Trail created by having a "Single Source of Truth"


If these outcomes sound like they might be relevant to your world, then I'd like to suggest we arrange a brief phone call. Together, we can quickly assess whether our companies would make for an ideal partnership. 

I will call your office on Tuesday, Feb 19 at 9:40 AM EST to make sure you've received this letter and answer any questions you may have. If this is an inconvenient time, kindly inform me as to your preferred availability.  

To your continued success,




Kevin McKinley

*****

An accomplished executive with expertise in B2C and B2B business and digital strategy, agile product development and design thinking, multi-million dollar P&L and product management. Proven ability to innovate, optimize end-to-end customer experiences, develop business compliance oversight, and implement product strategies that drives bottom-line results. Expertise in leveraging social media to drive brand relevance, reduce marketing expenses and generate revenue. Acknowledged as a people leader with integrity, credibility and passion for excellence.


Lead efforts to define and transform Key’s digital strategy for 3MM Community Bank clients as well as grow consumer deposit (Checking, Debit, fee income) and credit card products for $600MM+ P&L business.

Responsibilities include:
- Leading agile business delivery, digital sales strategy, capabilities roadmap, user experience and NPS measurement for consumer and small business digital experience
- Developing onboarding, loyalty and retention programs to reduce digital churn, improve client profitability; delivered record-level of digital sales productivity, new client growth, NPS and online/mobile active user utilisation
- Exceeding Household growth targets, growing faster than peers for 3 consecutive years by deploying new products, partnering with Branch network on productivity enhancements and improving customer experience


Jason Rudman joined Key in June 2014 and is the Director of Consumer Payments and Digital Banking for Key’s Community Bank. In this role, Jason is responsible for the strategic direction and profitable management of Key’s Consumer checking, debit, credit card and rewards products as well as leading Key’s Digital team in creating a vibrant, client-focused mobile and online experience. Together, he and his team serve the needs and interests of customers in each of the 15 markets that Key operates by delivering innovative solutions and tools to help Key clients lead a better financial life.

Prior to joining Key, Jason spent 13+ years at American Express in a variety of leadership positions focused on prepaid and small business P&L management, global product development, social media, credit card acquisition and interactive/digital customer experience and design. His most recent role was as general manager of American Express’ US Prepaid business, where among other things, he was part of the core team who developed and deployed Bluebird in partnership with Walmart. He also led the team responsible for the development and deployment of OPEN Forum, American Express’ award-winning experience dedicated to serving the needs of small business owners.

Jason serves on the Board of Trustees for ideastream/WVIX, as Chair of the Marketing and Development Committee. ideastream/WVIZ is a non-profit organization that applies the power of media to education, culture and citizenship. Based on careful and ongoing ascertainment of community needs, ideastream acquires, creates and delivers content that connects those who seek knowledge with those who have it. The services of ideastream multiple media are utilized by more than 2.8 million people a month in the Northeast Ohio region. Jason also served 3 years on the Board of The Ali Forney Center, the largest and most comprehensive program in the nation dedicated to meeting the needs of homeless LGBTQ youth.

Jason holds a Bachelor’s Degree in History from Middlesex University in London and a MBA from Baylor University. Jason and his husband live in downtown Cleveland and have two children.

