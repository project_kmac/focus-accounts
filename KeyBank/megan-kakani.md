* Megan Kakani - https://www.linkedin.com/in/megan-kakani-9b333215/
https://www.bankingtech.com/2018/09/the-perks-of-taking-accounts-receivable-processes-digital/


To: Megan Kakani, VP of Product and Innovation, Enterprise Commercial Payments 
KeyBank
Six Concourse Parkway
Atlanta, GA 30328


February 1, 2019

Subject: Why companies like Goldman Sachs use Gitlab to defend and secure all aspects of their Information Security

Warm greetings Ms. Kakani,

I hope the new year has treated you well. In my role as an SDR for GitLab, it is my responsibility to identify and research suitable partners who may benefit from our organization. 

During a recent interview with Straight Talk, you mentioned that Unum's digital transformation is focused on four key areas (customer experience, productivity and efficiency, expanding your ecosystem, and building out a growth platform). These initiatives are precisely what GitLab's application enables - and is why it is touted as being the fastest and most reliable way for CIOs to enable digital transformation throughout their respective organization.

As a concrete example, Goldman Sachs was in a similar scenario with regards to having a legacy environment -- which meant they couldn't attract the best talent and were slow to innovate. Over the last year, they've migrated 5,000 developers to GitLab, which has resulted in an 84x increase in build productivity and allowed them to be more nimble and produce faster than they ever thought possible.

*“Our teams went from two weeks releases to six times per day using GitLab because they do not need to wait for infrastructure." Andrew Knight, Managing Director, Goldman Sachs*

Mr. Bhasin, I believe there are at least five primary ways GitLab can help you drive value for your customers more quickly than your competitors.

1. We can enable better collaboration between IT and the business, resulting in alignment of your IT deliverables with business value and doing the right thing for the end customer  
2. We can improve your productivity and operational efficiency by having a Single Application for the entire Software Development Lifecycle, which means being able to produce new products significantly faster
3. We can help you be fiscally responsible by reducing IT costs and overhead which also means less time spent integrating and maintaining your tools
4. We can enhance information security and remediate vulnerabilities with automated testing and code scanning
5. We can make Regulatory Compliance easy with an IT Audit Trail created by having a "Single Source of Truth"

If these outcomes sound like they might be relevant to your world, then a brief phone call may be in order. Together, we can quickly assess whether your objectives and our solution would make for an ideal partnership. I will call your office on Friday, Feb 1 at 9:00 AM EST. If this is an inconvenient time, kindly inform me as to your preferred availability. 

To your continued success,

Kevin McKinley

