My professional background has focused on helping P&amp;C insurance organizations become more effective for over 25 years in consulting and leadership roles.

Specialties: Seasoned insurance leader that thrives in an environment where taking on challenges, solving complex problems, advancing innovation, and making a difference is the accepted norm.


Responsible for defining and implementing a global technology and operations strategic vision aligned with CNA’s growth and performance goals. Focused on delivering end-to-end solutions that optimize the company’s operations and further infuse digital, data and technology in all functions of the enterprise that ultimately increase the value received by agents, brokers and customers.


****

To: Joseph Merten, EVP, CNA Insurance
CNA Financial
333 S Wabash
Chicago, IL 60604 US


24 January 2019

Subject: Why companies like CHUBB and Goldman Sachs use Gitlab to adopt enterprise-wide digital transformation
 
Warm greetings and happy New Year, Mr. Merten,

I hope this missive finds you well. In my role as an SDR for GitLab, it is my responsibility to identify and research suitable partners who may benefit from our organization. 

During the last several months, I've learned by speaking with three of your IT Directors that CNA is moving to cloud native technologies and adopting DevOps through the enterprise. As these initiatives are precisely what our application enables there is (unsurpisingly) broad usage of GitLab at CNA. However, they also informed me that usage is limited to one aspect of the application and that CNA is not yet leveraging it's full breadth.

I bring this to your attention because I beleive that further utilization of GitLab's end-to-end solution can dramatically accelerate the delivery of your global technology operations and strategic vision. Those key areas of cloud native, DevOps, and enterprise-wide digital transformation are precisely why GitLab is used by clients like CHUBB.

As a concrete example, Goldman Sachs was in a similar situation looking to infuse technology in all functions of the enterprise. Over the last year, they've migrated 5,000 developers to GitLab, which has resulted in an 84x increase in build productivity and allowed them to deploy faster than they ever thought possible.

*“Our teams went from two weeks releases to six times per day using GitLab because they do not need to wait for infrastructure." Andrew Knight, Managing Director, Goldman Sachs*


Mr. Merten, I believe there are at least five primary ways GitLab can help you drive value for your customers more quickly than your competitors.

1. We can align your IT deliverables with business value and increase customer engagement through "Value Stream Mapping" and creating visibility into your pipeline
2. We can increase your operational efficiency, innovation velocity, and speed of delivery by having a Single Application for the entire Software Development Lifecycle
3. We can minimize IT costs and overhead which also means less time spent integrating and maintaining your tools 
4. We can address vulnerabilities and enhance information security with automated testing and code scanning
5. We can make Regulatory Compliance easy with an IT Audit Trail created by having a "Single Source of Truth"

If these outcomes sound like they might be relevant to your world, then a brief phone call may be in order. Together, we can quickly assess whether your objectives and our solution would make for an ideal partnership. I will call your office on Friday, Feb 1 at 9:00 AM EST. If this is an inconvenient time, kindly inform me as to your preferred availability. 

To your continued success,

Kevin McKinley
