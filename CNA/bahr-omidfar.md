To: Bahr Omidfar, CTO, CNA Insurance
CNA Financial
333 S Wabash
Chicago, IL 60604 US


24 January 2019

Subject: Why companies like Rockwell Automation and Goldman Sachs use Gitlab to adopt enterprise-wide digital transformation
 
Warm greetings and happy New Year, Mr. Omidfar,

I hope this missive finds you well. In my role as an SDR for GitLab, it is my responsibility to identify and research suitable partners who may benefit from our organization. 

During the last several months, I've learned by speaking with three of your IT Directors that you are moving to cloud native technologies and adopting DevOps through the enterprise. As these initiatives are precisely what our application enables there is (unsurpisingly) broad usage of GitLab at CNA. Surprisingly, that usage is limited to one aspect of the application (namely, source code management).

I bring this to your attention because I beleive that further utilization of GitLab's other features can dramatically accelerate the delivery of your proposed objectives. Those key areas of cloud native, DevOps, and enterprise-wide digital transformation (as you accomplished at Fidelity) are precisely why GitLab is used by clients like Rockwell Automation.

As a concrete example, Goldman Sachs was in a similar situation with an outdated techonology toolset -- which meant they couldn't attract the best talent and were slow to innovate. Over the last year, they've migrated 5,000 developers to GitLab, which has resulted in an 84x increase in build productivity and allowed them to deploy faster than they ever thought possible. (Teams that were releasing software every 2 weeks are now releasing 6x a day.)

*“Our teams went from two weeks releases to six times per day using GitLab because they do not need to wait for infrastructure." Andrew Knight, Managing Director, Goldman Sachs*


Mr. Omidfar, I believe there are at least five primary ways GitLab can help you drive value for your customers more quickly than your competitors.

1. We can align your IT deliverables with business value and increase customer engagement through "Value Stream Mapping" and creating visibility into your pipeline
2. We can increase your operational efficiency, innovation velocity, and speed of delivery by having a Single Application for the entire Software Development Lifecycle
3. We can minimize IT costs and overhead which also means less time spent integrating and maintaining your tools 
4. We can address vulnerabilities and enhance information security with automated testing and code scanning
5. We can make Regulatory Compliance easy with an IT Audit Trail created by having a "Single Source of Truth"

If these outcomes sound like they might be relevant to your world, then a brief phone call may be in order. Together, we can quickly assess whether your objectives and our solution would make for an ideal partnership. I will call your office on Friday, Feb 1 at 9:00 AM EST. If this is an inconvenient time, kindly inform me as to your preferred availability. 

To your continued success,

Kevin McKinley
