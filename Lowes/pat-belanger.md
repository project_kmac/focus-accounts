https://www.linkedin.com/in/pat-belanger-6563652/

To: Pat Belanger, VP IT Store Solutions, Lowe’s
1000 Lowe’s Boulevard
Mooresville, NC 28117
24 January 2018

Subject: Why companies like Macy’s and State Farm use Gitlab to adopt enterprise-wide digital transformation
 
Warm greetings and happy New Year, Mr. Belanger,

I hope this missive finds you well. In my role as an SDR for GitLab, it is my responsibility to identify and research suitable partners who may benefit from our organization. 

The initiatives you are engaged in to plan, deploy, and support Lowe’s software and solutions are precisely what the GitLab application enables. I believe our application can accelerate your delivery of these objectives, just as we've done for clients like Macy's.

GitLab is touted as being the fastest and most reliable way for VPs to enable digital transformation throughout their respective companies. As a concrete example, Goldman Sachs was in a similar situation with an outdated technology toolset -- which meant they couldn't attract the best talent and were slow to innovate. Over the last year, they've migrated 5,000 developers to GitLab, which has resulted in an 84x increase in build productivity and allowed them to deploy faster than they ever thought possible.

*“Our teams went from two weeks releases to six times per day using GitLab because they do not need to wait for infrastructure." Andrew Knight, Managing Director, Goldman Sachs*

Mr. Belanger, I believe there are at least six primary ways GitLab can help you drive value for your customers more quickly than your competitors.

1. We can align your business strategy and IT deliverables, allowing the advancement of ideas from innovation to commercialization by creating visibility in your deployment pipeline
2. We can increase your operational efficiency, innovation velocity, and speed of delivery by having a Single Application for the entire Software Development Lifecycle
3. We can reduce IT costs and overhead which means less time spent integrating and maintaining your tools so you can Do It Right For Less
4. We can ensure uptime through blue-green and canary deployments made easy with our cloud native integrations
5. We can address vulnerabilities and enhance information security with automated testing and code scanning
6. We can make Regulatory Compliance easy with an IT Audit Trail created by having a "Single Source of Truth"

If these outcomes sound like they might be relevant to your world, then a brief phone call may be in order. Together, we can quickly assess whether Lowe's and our solution are aligned for an ideal partnership. I will call your office on Friday, Feb 1 at 12:30 PM EST. If this is an inconvenient time, kindly inform me as to your preferred availability. 

To your continued success,

Kevin McKinley