https://www.linkedin.com/in/jamesbrandt/

To: James Brandt, Senior Vice President, Technology Infrastructure & Enterprise Services, Lowe’s
1000 Lowe’s Boulevard
Mooresville, NC 28117
24 January 2019

Subject: Why do companies like Goldman Sachs use Gitlab for the IT Infrastructure? Because it's the fastest way to adopt enterprise-wide DevOps digital transformation
 
Warm greetings and happy New Year, Mr. Brandt,

I hope this missive finds you well. I am reaching out to you today because in my role as an SDR for GitLab, it is my responsibility to identify and research suitable partners who may benefit from our organization. 

The initiatives you are engaged in to lead the IT Infrastructure to embrace Agile and DevOps principles, as well as PAAS and IAAS cloud capabilities, are precisely what the GitLab application enables. I believe our application can accelerate your delivery of these objectives, just as we've done for clients like Macy's.

GitLab is touted as being the fastest and most reliable way for SVPs to enable digital transformation throughout their respective companies. As a concrete example, Goldman Sachs was in a similar situation with an outdated technology toolset -- which meant that they were slow to innovate and couldn't attract the best talent. Over the last year, they've migrated 5,000 developers to GitLab, which has resulted in an 84x increase in build productivity and allowed them to deploy faster than they ever thought possible.

*“Our teams went from two weeks releases to six times per day using GitLab because they do not need to wait for infrastructure." Andrew Knight, Managing Director, Goldman Sachs*

Mr. Brandt, I believe there are at least five primary ways GitLab can help you drive value for your customers more quickly than your competitors.

1. We can align your IT deliverables with business value and increase customer engagement through "Value Stream Mapping" and creating visibility into your pipeline
2. We can increase your operational efficiency, innovation velocity, and speed of delivery by having a Single Application for the entire Software Development Lifecycle
3. We can reduce IT costs and overhead which means less time spent integrating and maintaining your tools so you can Do It Right For Less
4. We can address vulnerabilities and enhance information security with automated testing and code scanning
5. We can make Governance Reporting and Regulatory Compliance easy with an IT Audit Trail created by having a "Single Source of Truth"

If these outcomes sound like they might be relevant to your world, let’s arrange a brief phone call to see if our companies would make for a great partnership. I will call your office on Friday, Feb 1 at 9:30 AM EST. If this is an inconvenient time, kindly inform me as to your preferred availability. 

To your continued success,

Kevin McKinley

****

