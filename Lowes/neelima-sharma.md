https://www.linkedin.com/in/neelimasharma/


Neelima Sharma, VP of IT E-Commerce, Lowe’s
1000 Lowe’s Boulevard
Mooresville, NC 28117
24 January 2018

Subject: Why the VPs of E-Commerce companies like Macy’s and Expedia use Gitlab to adopt enterprise-wide digital transformation
 
Warm greetings and happy New Year, Ms. Sharma,

I hope this missive finds you well. I am writing to you today because in my role as an SDR for GitLab, it is my responsibility to identify and research suitable partners who may benefit from our organization. 

The initiatives you are engaged in to improve Lowe's site stability and speed, as well as quickly launching and scaling features through a new technology platform, are precisely what GitLab enables. 

GitLab is touted as being the fastest and most reliable way for VPs to enable digital transformation throughout their respective companies. Here’s why…

GitLab is an application is used by over 100,000 enterprises worldwide to develop software, modernize their architecture, embrace cloud-native technologies, and increase velocity of innovation.

Ms. Sharma, I believe there are at least five primary ways GitLab can help you drive revenue, increase efficiency, and improve Lowe's market share:


1. We can improve your customer engagement (especially with the Pros) by aligning your IT deliverables with what the customers want
2. We can increase your operational efficiency, innovation velocity, and speed of delivery by having a Single Application for the entire Software Development Lifecycle
3. We can reduce IT costs and overhead which means less time spent integrating and maintaining your tools so you can Do It Right For Less
4. We can address vulnerabilities and enhance information security with automated testing and code scanning
5. We can make Regulatory Compliance easy with an IT Audit Trail created by having a "Single Source of Truth"

If these outcomes sound like they might be relevant to your world, let’s arrange a brief phone call to see if our companies would make for a great partnership. I will call your office on Friday, Feb 1 at 11:30 AM EST. If this is an inconvenient time, kindly inform me as to your preferred availability. 

To your continued success,

Kevin McKinley
