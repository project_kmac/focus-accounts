
To: Warren Steytler, CISO, Lowe’s
1000 Lowe’s Boulevard
Mooresville, NC 28117
24 January 2019

Subject: Why the CISOs of companies like Macy’s and Expedia use Gitlab to adopt enterprise-wide digital transformation
 
Warm greetings and happy New Year, Mr. Steytler,

I hope this missive finds you well, and congratulations in acquiring your US Citizenship. 

I am reaching out to you today because in my role as an SDR for GitLab, it is my responsibility to identify and research suitable partners who may benefit from our organization. 

The initiatives you are engaged in to secure Lowe’s technology infrastructure and protect critical customer information are precisely what the GitLab application enables. GitLab is touted as being the fastest and most reliable way for CISOs to shift security left throughout their respective companies.

I bring this to your attention because  

Mr. Steytler, I believe there are at least four primary ways GitLab can help you defend and secue information technology while still driving value for your customers.

1. We can create visibility into your entire pipeline by having a Single Application for the entire Software Development Lifecycle
2. We can minimize IT costs and overhead which means less time spent integrating and maintaining your tools so you can Do It Right For Less 
3. We can address vulnerabilities and enhance information security with automated testing and code scanning
4. We can make Regulatory Compliance easy with an IT Audit Trail created by having a "Single Source of Truth"

1. With GitLab, you no longer need choose between velocity and risk. Because it’s not an incremental cost, you can test every code change, not just critical apps or annual scans. You can test ALL of your code, on every commit, automatically, with real time provisioning.
2. We can help you better leverage scarce security resources by putting application security tools, that are meant for the developer, into the hands of the developer. This means that those 10,000 hands on a keyboard can remediate more code and earlier than possible with traditional application security tools.
3. We can make regulatory compliance and governance reporting easy with an IT Audit Trail created by having a "Single Source of Truth"

If these outcomes sound like they might be relevant to your world, may I suggest we arrange a brief phone call? Together, we can quickly assess whether our companies would make for an ideal partnership. 

I will call your office on Tuesday, Feb 19 at 9:40 AM EST to make sure you've received this letter and answer any questions you may have. If this is an inconvenient time, kindly inform me as to your preferred availability.  

To your continued success,

Kevin McKinley
