# Focus Account

## Company Background:

https://www.prnewswire.com/news-releases/atd-announces-go-forward-innovation-strategy-300671663.html
https://www.prnewswire.com/news-releases/american-tire-distributors-issues-open-letter-to-stakeholders-300635800.html
https://www.prnewswire.com/news-releases/american-tire-distributors-successfully-completes-recapitalization-process-300770453.html

"Today marks a new beginning for ATD. We are a stronger company with the financial flexibility to build on an 80-year history of leadership and innovation," said Stuart Schuette, Chief Executive Officer of ATD. "We are laser-focused on delivering greater value for our customers and manufacturer partners, with differentiated capabilities based on advanced analytics. Our ongoing transformation means that ATD will remain the most connected and insightful automotive solutions provider for decades to come."

https://www.atd-us.com/en/news-article/ryan-marsh

“The world is changing,” said Stuart Schuette, president and CEO of ATD, in his presentation at the Tire Pros event. “We need to stay ahead of that growth in innovation. We actually have now created a company within a company; we have a growth and innovation organization put in place at ATD. Their focus is on how to help you as an independent dealer.” "separate company thinking about what we need to do to build things out for our dealer partners"

https://www.tirebusiness.com/article/20180604/NEWS/180609995/atds-stuart-schuette-tire-distributor-strong-despite-recent-setbacks

Mr. Schuette said ATD's goal for the next six months is to become an intelligent digital distributor for retailers and manufacturers, connecting consumers to dealers, creating an "efficient digital ecosystem."

The firm plans to accomplish that by:

Investing in knowledge;
Monitoring the market;
Leveraging exceptional commercial capabilities;
Boosting manufacturing technology; and
Embedding technology to create competitive advantages in the long term.
"We're excited about the future, particularly in the area of digital technology," Mr. Schuette said. "As we look ahead, we'll be driving game-changing innovation. ATD is — and will continue to be — the leader in the replacement tire business because of our commitment to focusing on innovation to deliver the very best experience for our customers.

"We know we can achieve that through the continued dedication of our associates. We can't wait for tomorrow."


## Account Research:

Why's building & execution: How effectively do you determine the 3 Why's of doing a deal?

1. Why Gitlab - They are at the beginning of their DevOps Journey. 

2. Why Now - Filed for Chapter 11 Bankruptcy in Q4 2018 to reduce debt by $1.1 billion. Stuart Schuette includes innovation as part of his turn around initiatives (and why Ryan Marsh was brought in as Chief Growth and Innovation) 

3. Why Do Anything At All?



## Identify C-Suite:

1. Stuart Schuette - CEO https://www.linkedin.com/in/stuartschuette/
2. Ivy Chin, CDO/CTO 
3. Kimberly Trappani - Chief Digital Security Officer and CISO
4. Keith Calcagno - Chief Transformation & Strategy Officer 
5. Murali Bandaru, SVP of APP DEV
6. Mike Francischiello, VP of Infrastructure Services & Operations
7. Ryan Marsh, Growth Chief & Innovation Officer https://www.linkedin.com/in/ryan-marsh-a96bb71/

* David Maillet, Director of DevOps, QA, Performance Engineering
* Stephen Tate
* Benjamin Jackson Director, Enterprise Architecture - Moved to Ahold Delhaize
* Ravi Kantamneni, Director of eCommerce Delivery
* Rob Hegelson, Director of Innovation (previously eCommerce Delivery)



## Call Log:

* What do you know about Gitlab?
* Role
* Size 
* Situation
* Problem 
* Goals
* Budget
* Authority
* Timeline	
* Questions
* Next Steps



Activity History for David Maillet: https://na34.salesforce.com/007?id=0036100001gOMcq&rlid=RelatedHistoryList&closed=1

Ivy Chin's Shunt to David Maillet: https://na34.salesforce.com/00T6100002DcDRF

Prashanthi Srinivasan calls:
* Feb 21 - https://api.twilio.com/2010-04-01/Accounts/AC881d04f3540c44cc4545bf49c9aaf04b/Recordings/REf06e15a495a9ddb65bd107546c16ee88.wav
* Novemeber 15 - https://api.twilio.com/2010-04-01/Accounts/AC881d04f3540c44cc4545bf49c9aaf04b/Recordings/RE1b0d93c57d3128c5229369d69be02c19.wav Brush off to email. I was too quick to ask for the appt without building value. Off script on 30 sec commercial.

